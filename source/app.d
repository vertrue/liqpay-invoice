import vibe.utils.dictionarylist;
import std.stdio;
import std.uuid;
import std.string;
import std.conv;
import vibe.http.server;
import vibe.http.router;
import core.time;
import vibe.core.core;
import vibe.core.log;
import vibe.data.json;
import vibe.http.client;
import std.datetime.systime;
import std.file;
import std.net.curl;
import std.json;
import std.ascii : LetterCase;
import std.digest;
import std.digest.md;
import std.digest.sha;
import std.string : representation;
import std.digest.hmac;
import std.base64;
import vibe.data.serialization;
import std.uri;
import mongodb;

Database db;

void main()
{
	db = new Database("liqpay_invoices", "", "");

	auto router = new URLRouter;
	router
		.post("/newInvoice",&newInvoice)
		.post("/invoiceStatus",&invoiceStatus);
	auto l = listenHTTP("0.0.0.0:3229", router);
	runApplication();
	scope (exit) {
	    exitEventLoop(true);
	    sleep(1.msecs);
    }
    l.stopListening();
}

void newInvoice(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	double amount = req_end.json["amount"].to!double;
	string public_key = req_end.json["public_key"].to!string;
	string private_key = req_end.json["private_key"].to!string;
	string phone = req_end.json["phone"].to!string;
	string bot_id_ = req_end.json["bot_id"].to!string;
	string chatfuel_token_ = req_end.json["chatfuel_token"].to!string;
	string user_id = req_end.json["user_id"].to!string;
	string success = req_end.json["suc_block"].to!string;
	string error = req_end.json["err_block"].to!string;
	string pending = req_end.json["pen_block"].to!string;
	string description = req_end.json["description"].to!string;
	string service_url = req_end.json["service_url"].to!string;


	string order = randomUUID().toString().replace("-" , "");
	Json post = Json.emptyObject;
	post["version"] = 3;
	post["order_id"] = order;
	post["public_key"] = public_key;
	post["action"] = "invoice_bot";
	post["amount"] = amount;
	post["currency"] = "UAH";
	post["description"] = description;
	post["phone"] = phone;
	post["server_url"] = service_url;

	ubyte[] b = cast(ubyte[])(post.toString().dup);
	string data = Base64.encode(b);

	string signature = Base64.encode(sha1Of(private_key ~ data ~ private_key));

	requestHTTP("https://www.liqpay.ua/api/request",					
		(scope req) {
			req.method = HTTPMethod.POST;

			Json a = Json.emptyObject;
			a["public_key"] = public_key;
			a["private_key"] = private_key;
			a["bot_id"] = bot_id_;
			a["chatfuel_token"] = chatfuel_token_;
			a["user_id"] = user_id;
			a["order"] = order;
			a["success"] = success;
			a["error"] = error;
			a["pending"] = pending;
			a["description"] = description;
			db["workspace_invoices"].insert(a);

			req.writeBody(cast(ubyte[])("data=" ~ data ~ "&signature=" ~ signature.dup),"application/x-www-form-urlencoded");
		},
		(scope res) {
			Json resp = res.readJson;
			string final_url = resp["href"].get!string;
			Json ans = parseJsonString("{\"set_attributes\":{\"invoice_url\":\"" ~ final_url ~ "\"}}");
			res_end.writeJsonBody(ans);
		}
	);
	

}

void invoiceStatus(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
   	Json requ = req_end.json;
	string order = requ["order_id"].to!string, user_id, bot_id, chatfuel_token;
	if(db["workspace_invoices"].find("order", order).length)
	{
		Json invoice = db["workspace_invoices"].find("order", order)[0];
		user_id = invoice["user_id"].to!string;
		bot_id = invoice["bot_id"].to!string;
		chatfuel_token = invoice["chatfuel_token"].to!string;
		string success, error, pending;
		success = invoice["success"].to!string;
		error = invoice["error"].to!string;
		pending = invoice["pending"].to!string;

		if(requ["status"].to!string == "success")
			requestHTTP("https://api.chatfuel.com/bots/" ~ bot_id ~ "/users/" ~ user_id ~ "/send?chatfuel_token=" ~ chatfuel_token ~ "&chatfuel_block_id=" ~ success,					
				(scope req) {
					req.method = HTTPMethod.POST;
				},
				(scope res){
				}
			);
		else if(requ["status"].to!string == "processing")
			requestHTTP("https://api.chatfuel.com/bots/" ~ bot_id ~ "/users/" ~ user_id ~ "/send?chatfuel_token=" ~ chatfuel_token ~ "&chatfuel_block_id=" ~ pending,					
				(scope req) {
					req.method = HTTPMethod.POST;
				},
				(scope res){
				}
			);
		else if(requ["status"].to!string == "error" || requ["status"].to!string == "failure")
			requestHTTP("https://api.chatfuel.com/bots/" ~ bot_id ~ "/users/" ~ user_id ~ "/send?chatfuel_token=" ~ chatfuel_token ~ "&chatfuel_block_id=" ~ error,					
				(scope req) {
					req.method = HTTPMethod.POST;
				},
				(scope res){
				}
			);
	}
}
